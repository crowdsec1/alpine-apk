data "terraform_remote_state" "ec2" {
    backend = "s3"
    config  = {
        bucket = "crowsec"
        key    = "ec2/terraform.tfstate"
        region = "eu-west-1"
    }
}

data "terraform_remote_state" "vpc" {
    backend = "s3"
    config  = {
        bucket = "crowsec"
        key    = "vpc/terraform.tfstate"
        region = "eu-west-1"
    }
}