provider "aws" {
    region = "eu-west-1"
}

# aws_instance.crowdsec-instance:
resource "aws_instance" "crowdsec-instance" {
    ami                          = "ami-058b1b7fe545997ae"
    associate_public_ip_address  = true
    availability_zone            = "eu-west-1b"
    cpu_core_count               = 1
    cpu_threads_per_core         = 2
    disable_api_termination      = false
    ebs_optimized                = true
    get_password_data            = false
    hibernation                  = false
    instance_type                = "t3.micro"
    ipv6_address_count           = 0
    ipv6_addresses               = []
    key_name                     = "ansible"
    monitoring                   = false
   

    private_ip                   = "10.0.0.163"

    secondary_private_ips        = []
    security_groups              = []
    source_dest_check            = true
    subnet_id                    = "subnet-06b2ec6bd3993b900"
    tags                         = {
        "APP"    = "alpine-apk"
        "Manage" = "Terraform"
        "Projet" = "Crowdsec"
    }
    tenancy                      = "default"
    vpc_security_group_ids       = [
        "sg-0a2ca51824cd629e1",
    ]

    credit_specification {
        cpu_credits = "unlimited"
    }

    ebs_block_device {
        delete_on_termination = false
        device_name           = "/dev/sdf"

        encrypted             = false
        iops                  = 100
        tags                  = {
            "APP"    = "alpine-apk"
            "Manage" = "Terraform"
            "Projet" = "Crowdsec"
        }
        throughput            = 0
        volume_size           = 8
        volume_type           = "gp2"
    }

    enclave_options {
        enabled = false
    }

    metadata_options {
        http_endpoint               = "enabled"
        http_put_response_hop_limit = 1
        http_tokens                 = "optional"
    }

    root_block_device {
        delete_on_termination = true

        encrypted             = false
        iops                  = 100
        tags                  = {
            "APP"    = "alpine-apk"
            "Manage" = "Terraform"
            "Projet" = "Crowdsec"
        }
        throughput            = 0

        volume_size           = 8
        volume_type           = "gp2"
    }

    timeouts {}
}
