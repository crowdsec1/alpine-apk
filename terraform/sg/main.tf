provider "aws" {
    region = "eu-west-1"
}

# aws_security_group.crowdsec-sg:
resource "aws_security_group" "crowdsec-sg" {
    egress      = [
        {
            cidr_blocks      = [
                "0.0.0.0/0",
            ]
            description      = ""
            from_port        = 0
            ipv6_cidr_blocks = []
            prefix_list_ids  = []
            protocol         = "-1"
            security_groups  = []
            self             = false
            to_port          = 0
        },
    ]
    ingress     = [
        {
            cidr_blocks      = [
                "78.202.168.244/32",
            ]
            description      = ""
            from_port        = -1
            ipv6_cidr_blocks = []
            prefix_list_ids  = []
            protocol         = "icmp"
            security_groups  = []
            self             = false
            to_port          = -1
        },
        {
            cidr_blocks      = [
                "78.202.168.244/32",
            ]
            description      = ""
            from_port        = 10250
            ipv6_cidr_blocks = []
            prefix_list_ids  = []
            protocol         = "tcp"
            security_groups  = []
            self             = false
            to_port          = 10250
        },
        {
            cidr_blocks      = [
                "78.202.168.244/32",
            ]
            description      = ""
            from_port        = 10251
            ipv6_cidr_blocks = []
            prefix_list_ids  = []
            protocol         = "tcp"
            security_groups  = []
            self             = false
            to_port          = 10251
        },
        {
            cidr_blocks      = [
                "78.202.168.244/32",
            ]
            description      = ""
            from_port        = 10252
            ipv6_cidr_blocks = []
            prefix_list_ids  = []
            protocol         = "tcp"
            security_groups  = []
            self             = false
            to_port          = 10252
        },
        {
            cidr_blocks      = [
                "78.202.168.244/32",
            ]
            description      = ""
            from_port        = 22
            ipv6_cidr_blocks = []
            prefix_list_ids  = []
            protocol         = "tcp"
            security_groups  = []
            self             = false
            to_port          = 22
        },
        {
            cidr_blocks      = [
                "78.202.168.244/32",
            ]
            description      = ""
            from_port        = 2379
            ipv6_cidr_blocks = []
            prefix_list_ids  = []
            protocol         = "tcp"
            security_groups  = []
            self             = false
            to_port          = 2380
        },
        {
            cidr_blocks      = [
                "78.202.168.244/32",
            ]
            description      = ""
            from_port        = 23
            ipv6_cidr_blocks = []
            prefix_list_ids  = []
            protocol         = "tcp"
            security_groups  = []
            self             = false
            to_port          = 23
        },
        {
            cidr_blocks      = [
                "78.202.168.244/32",
            ]
            description      = ""
            from_port        = 37057
            ipv6_cidr_blocks = []
            prefix_list_ids  = []
            protocol         = "tcp"
            security_groups  = []
            self             = false
            to_port          = 37057
        },
        {
            cidr_blocks      = [
                "78.202.168.244/32",
            ]
            description      = ""
            from_port        = 443
            ipv6_cidr_blocks = []
            prefix_list_ids  = []
            protocol         = "tcp"
            security_groups  = []
            self             = false
            to_port          = 443
        },
        {
            cidr_blocks      = [
                "78.202.168.244/32",
            ]
            description      = ""
            from_port        = 6443
            ipv6_cidr_blocks = []
            prefix_list_ids  = []
            protocol         = "tcp"
            security_groups  = []
            self             = false
            to_port          = 6443
        },
        {
            cidr_blocks      = [
                "78.202.168.244/32",
            ]
            description      = ""
            from_port        = 80
            ipv6_cidr_blocks = []
            prefix_list_ids  = []
            protocol         = "tcp"
            security_groups  = []
            self             = false
            to_port          = 80
        },
        {
            cidr_blocks      = [
                "78.202.168.244/32",
            ]
            description      = ""
            from_port        = 8443
            ipv6_cidr_blocks = []
            prefix_list_ids  = []
            protocol         = "tcp"
            security_groups  = []
            self             = false
            to_port          = 8443
        },
    ]
    name        = "kubesecurity"
    vpc_id      = "vpc-00b6fa8430403bfb8"

    timeouts {}
}
