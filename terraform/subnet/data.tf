data "terraform_remote_state" "vpc" {
    backend = "s3"
    config  = {
        bucket = "crowsec"
        key    = "vpc/terraform.tfstate"
        region = "eu-west-1"
    }
}

data "terraform_remote_state" "subnet" {
    backend = "s3"
    config  = {
        bucket = "crowsec"
        key    = "subnet/terraform.tfstate"
        region = "eu-west-1"
    }
}