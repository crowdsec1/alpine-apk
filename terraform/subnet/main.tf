provider "aws" {
    region = "eu-west-1"
}

resource "aws_subnet" "crowdsec-subnet" {
    availability_zone               = "eu-west-1b"
    cidr_block                      = "10.0.0.128/26"
    map_public_ip_on_launch         = false

    vpc_id = "vpc-00b6fa8430403bfb8"

    timeouts {}
}
