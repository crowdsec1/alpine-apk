provider "aws" {
    region = "eu-west-1"
}

resource "aws_vpc" "crowdsec-vpc" {

    assign_generated_ipv6_cidr_block = false
    cidr_block                       = "10.0.0.0/24"
    enable_classiclink               = false
    enable_classiclink_dns_support   = false
    enable_dns_hostnames             = false
    enable_dns_support               = true
    instance_tenancy                 = "default"


}
